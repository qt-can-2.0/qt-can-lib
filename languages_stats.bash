#!/bin/bash

function countTotalSize {
    RET=0
    SIZES_LIST=$(find . -name "$1" -type f  -exec stat -c%s {} \;)
    for NUM in $SIZES_LIST
    do
        RET=$(($RET + $NUM))
    done
    echo $RET
}

function div {  # Arguments: dividend and divisor
    if [ $2 -eq 0 ]
    then
        echo division by 0
        exit
    fi
    local p=12                            # precision
    local c=${c:-0}                       # precision counter
    local d=.                             # decimal separator
    local r=$(($1/$2)); echo -n $r        # result of division
    local m=$(($r*$2))
    [ $c -eq 0 ] && [ $m -ne $1 ] && echo -n $d
    [ $1 -eq $m ] || [ $c -eq $p ] && return
    local e=$(($1-$m))
    let c=c+1
    div $(($e*10)) $2
}

TOTAL_HPP=$(countTotalSize "*.h")
TOTAL_CPP=$(countTotalSize "*.cpp")
TOTAL_QML=$(countTotalSize "*.qml")
TOTAL_JS=$(countTotalSize "*.js")
TOTAL_QBS=$(countTotalSize "*.qbs")
TOTAL_SVG=$(countTotalSize "*.svg")
TOTAL_PRO=$(countTotalSize "*.pro")
TOTAL_PRI=$(countTotalSize "*.pri")
TOTAL_QRC=$(countTotalSize "*.qrc")

GRAND_TOTAL=$(($TOTAL_CPP + $TOTAL_HPP + $TOTAL_QBS + $TOTAL_QML + $TOTAL_SVG + $TOTAL_PRI + $TOTAL_PRO + $TOTAL_JS + $TOTAL_QRC))

PERCENT_CPP_H=$(div $(( ($TOTAL_CPP + $TOTAL_HPP) * 100)) $GRAND_TOTAL )
PERCENT_JS=$(div    $((  $TOTAL_JS                * 100)) $GRAND_TOTAL )
PERCENT_QML=$(div   $((  $TOTAL_QML               * 100)) $GRAND_TOTAL )
PERCENT_QBS=$(div   $((  $TOTAL_QBS               * 100)) $GRAND_TOTAL )
PERCENT_QMAKE=$(div $(( ($TOTAL_PRI + $TOTAL_PRO) * 100)) $GRAND_TOTAL )
PERCENT_SVG=$(div   $((  $TOTAL_SVG               * 100)) $GRAND_TOTAL )
PERCENT_QRC=$(div   $((  $TOTAL_QRC               * 100)) $GRAND_TOTAL )

OUTPUT=""
if [ "$PERCENT_CPP_H" != "0" ]
then
    if [ "$OUTPUT" != "" ]
    then
        OUTPUT=$OUTPUT$'\n'
    fi
    OUTPUT=$OUTPUT"$PERCENT_CPP_H C++"
fi

if [ "$PERCENT_QML" != "0" ]
then
    if [ "$OUTPUT" != "" ]
    then
        OUTPUT=$OUTPUT$'\n'
    fi
    OUTPUT=$OUTPUT"$PERCENT_QML QML"
fi

if [ "$PERCENT_JS" != "0" ]
then
    if [ "$OUTPUT" != "" ]
    then
        OUTPUT=$OUTPUT$'\n'
    fi
    OUTPUT=$OUTPUT"$PERCENT_JS JS"
fi

if [ "$PERCENT_QBS" != "0" ]
then
    if [ "$OUTPUT" != "" ]
    then
        OUTPUT=$OUTPUT$'\n'
    fi
    OUTPUT=$OUTPUT"$PERCENT_QBS QBS"
fi

if [ "$PERCENT_QMAKE" != "0" ]
then
    if [ "$OUTPUT" != "" ]
    then
        OUTPUT=$OUTPUT$'\n'
    fi
    OUTPUT=$OUTPUT"$PERCENT_QMAKE QMake"
fi

if [ "$PERCENT_QRC" != "0" ]
then
    if [ "$OUTPUT" != "" ]
    then
        OUTPUT=$OUTPUT$'\n'
    fi
    OUTPUT=$OUTPUT"$PERCENT_QRC QRC"
fi

if [ "$PERCENT_SVG" != "0" ]
then
    if [ "$OUTPUT" != "" ]
    then
        OUTPUT=$OUTPUT$'\n'
    fi
    OUTPUT=$OUTPUT"$PERCENT_SVG SVG"
fi

echo "-------------------------------"
echo "| Stats of text files by size |"
echo "==============================="
echo "$OUTPUT" | sort -r -n  | awk '{printf "| %-15s | %7.3f % |\n",$2,$1}'
echo "-------------------------------"

exit 0
