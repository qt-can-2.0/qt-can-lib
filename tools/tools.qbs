import qbs;
import qbs.File;

Project {
    name: "tools";
    references: {
        var ret = [];
        for (var idx = 0; idx < subProjects.length; idx++) {
            var subProjectFilePath = subProjects [idx];
            if (File.exists (sourceDirectory + "/" + name + "/" + subProjectFilePath)) {
                ret.push (subProjectFilePath);
            }
        }
        return ret;
    }

    readonly property stringList subProjects : [
        "CanSniffer/CanSniffer.qbs",
        "CanBridge/CanBridge.qbs",
        "CanTestBench/CanTestBench.qbs",
        "CanOpenObdExport/CanOpenObdExport.qbs",
        "CanOpenNodeTestUi/CanOpenNodeTestUi.qbs",
    ];
}
