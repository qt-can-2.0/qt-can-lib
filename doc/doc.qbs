import qbs;

Project {
    name: "doc";

    Product {
        name: (project.namePrefixBase + "wiki");

        Group {
            name: "Images";
            files: [
                "logo.png",
                "logo.svg",
            ]
        }
    }
    Product {
        name: (project.namePrefixBase + "doxygen");

        Group {
            name: "Doxyfiles";
        }
    }
}
