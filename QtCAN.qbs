import qbs;
import qbs.File;

Project {
    name: "Qt CAN";
    minimumQbsVersion: "1.7.0";
    references: {
        var ret = [];
        for (var idx = 0; idx < subProjects.length; idx++) {
            var subProjectFilePath = subProjects [idx];
            if (File.exists (sourceDirectory + "/" + subProjectFilePath)) {
                ret.push (subProjectFilePath);
            }
        }
        return ret;
    }

    property bool baseAsSharedLib       : true;
    property bool utilsAsSharedLib      : true;
    property bool driversAsPlugins      : true;
    property bool protocolsAsSharedLibs : true;

    readonly property string namePrefixBase      : "libqtcan-";
    readonly property string namePrefixTests     : (namePrefixBase + "test-");
    readonly property string namePrefixTools     : (namePrefixBase + "tool-");
    readonly property string namePrefixDrivers   : (namePrefixBase + "driver-");
    readonly property string namePrefixProtocols : (namePrefixBase + "protocol-");

    readonly property string targetPrefixBase      : "QtCAN-";
    readonly property string targetPrefixTests     : (targetPrefixBase + "test-");
    readonly property string targetPrefixTools     : (targetPrefixBase + "tool-");
    readonly property string targetPrefixDrivers   : (targetPrefixBase + "driver-");
    readonly property string targetPrefixProtocols : (targetPrefixBase + "protocol-");

    readonly property string installDirDrivers   : "drivers";
    readonly property string installDirLibraries : (qbs.targetOS.contains ("linux") ? "lib" : "");

    readonly property stringList subProjects : [
        "doc/doc.qbs",
        "3rdparty/3rdparty.qbs",
        "canbus/canbus.qbs",
        "drivers/drivers.qbs",
        "protocols/protocols.qbs",
        "tools/tools.qbs",
        "utils/utils.qbs",
    ];
}
